package crawler

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"os"
	"strings"
	"testing"
)

func main() {
}

var itviecParser ItviecParser

func TestItViecParser_ParseJob(t *testing.T) {
	var emptyJob = itviecParser.ParseJobFromDocument(nil)
	if (emptyJob != nil) {
		fmt.Println("Empty job failed")
		t.Fail()
	}

	var simpleFile, err = os.Open("sample_job.html")
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}

	var doc, _ = goquery.NewDocumentFromReader(simpleFile)
	var simpleJob = itviecParser.ParseJobFromDocument(doc)
	if simpleJob == nil {
		fmt.Println("Nil returned")
		t.Failed()
	}
	if simpleJob.Title != "Sr. Frontend Dev. (JavaScript/HTML5/CSS)" {
		fmt.Println("Wrong title")
		t.Failed()
	}
	if !strings.Contains(simpleJob.Benefits, "Opportunity fly to Australia") ||
		!strings.Contains(simpleJob.Benefits, "Work alongside with CEO, CTO") {
		fmt.Println("Wrong benefits")
		t.Fail()
	}
	if !strings.Contains(simpleJob.Description, "As a Front End Engineer you will") {
		fmt.Println("Wrong descriptions")
		t.Fail()
	}
	if !StringInArray("HTML5", simpleJob.Tags) ||
		!StringInArray("JavaScript", simpleJob.Tags) {
			fmt.Println(len(simpleJob.Tags))
			fmt.Println("Wrong tags")
			t.Fail()
	}


}
