package crawler

import (
	"github.com/PuerkitoBio/goquery"
	"strings"
)

type Parser interface {
	ParseJob(string) *Job
}

type ItviecParser struct {}

func (p *ItviecParser) ParseJob(s string) *Job {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(s))
	if err != nil {
		println(err)
		return nil
	}
	return p.ParseJobFromDocument(doc)
}

func (p *ItviecParser) ParseJobFromDocument(html *goquery.Document) *Job {
	if html == nil {
		return nil
	}
	var job Job
	job.Title = strings.TrimSpace(html.Find(".title").First().Text())
	html.Find(".benefits").First().
		ChildrenFiltered("li").Each(func(i int, selection *goquery.Selection) {
			job.Benefits += strings.TrimSpace(selection.Text()) + "\n"
	})
	job.Description = strings.TrimSpace(
		strings.Replace(html.Find(".description").First().Text(),"\n"," ",0))

	html.Find(".tag-list").First().
		ChildrenFiltered("a").Each(func(i int, selection *goquery.Selection) {
			job.Tags = append(job.Tags, strings.TrimSpace(selection.Text()))
	})


	return &job
}