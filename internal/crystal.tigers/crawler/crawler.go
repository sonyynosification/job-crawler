package crawler

import (
	"fmt"
	"github.com/gocolly/colly"
	"log"
	url2 "net/url"
	"time"
)

type Crawler interface {
	Crawl(string, chan Job)
}

type CollyCrawler struct {}

func (cr *CollyCrawler) Crawl(rawurl string, chn chan Job) {
	// Check URL is valid
	url, err := url2.Parse(rawurl)
	if err != nil {
		log.Fatal(err)
		return
	}

	var parser Parser
	if url.Host == DomainItviec {
		parser  = &ItviecParser{}
	}

	c := colly.NewCollector(
		colly.AllowedDomains(url.Host),
		colly.MaxDepth(2),
		//colly.Debugger(&debug.LogDebugger{}),
		//colly.Async(true),
	)

	c.Limit(&colly.LimitRule{
		DomainGlob: "*",
		//Parallelism: 2,
		RandomDelay: 10 * time.Second,
	})

	c.OnHTML(".first-group .job", func (e *colly.HTMLElement) {
		chn <- *parser.ParseJob(e.Text)
	})

	// Find jobs
	//c.OnHTML(".first-group .job .job_content .job__description .job__body .details .title a", func(e *colly.HTMLElement) {
	//	//PrintJobs(e)
	//})

	// Find and visit more jobs links
	c.OnHTML("a.more-jobs-link", func(e *colly.HTMLElement) {

		//e.Request.Visit(e.Attr("href"))
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	err = c.Visit(rawurl)
	if err != nil {
		fmt.Println(err)
		return
	}
	c.Wait()
	close(chn)
}

var VCrawler Crawler = &CollyCrawler{}

func Crawl(url string) {
	c := make(chan Job, 100)
	go VCrawler.Crawl(url, c)

	for i := range c {
		fmt.Println(i.ToString())
	}
}