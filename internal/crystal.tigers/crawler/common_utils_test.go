package crawler

import "testing"

func TestStringInArray(t *testing.T) {
	arr1 := []string{"AB","Ac", "bc"}
	if StringInArray("A", arr1) {
		t.Failed()
	}
	if !StringInArray("Ac", arr1) {
		t.Failed()
	}
	if StringInArray("bC", arr1) {
		t.Failed()
	}
}
