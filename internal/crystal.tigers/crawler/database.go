package crawler

import (
	_ "github.com/go-sql-driver/mysql"
	"log"
)

import "database/sql"

type DBDriver interface {
	Connect(prop DBConnectionProperties) (interface{}, error)
}

type MysqlDB struct {
	Conn *sql.DB
}

type DBConnectionProperties struct {
	User string
	Password string
	Host string
	Port int
	DbName string
	Driver string
}

func (driver *MysqlDB) Connect(prop DBConnectionProperties) (*sql.DB, error){
	conn, err := sql.Open("mysql", prop.User + ":" + prop.Password + "@/" + prop.DbName)
	driver.Conn = conn
	return conn, err
}

func (driver *MysqlDB) AddJob(job Job) error {
	tx, err := driver.Conn.Begin()
	if err != nil {
		log.Fatal(err)
	}
	defer tx.Rollback()
	stmt, err := tx.Prepare("INSERT INTO JOBS (title, description, salary, location, source, source_id) VALUE(?,?,?,?,?,?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(job.Title, job.Description, job.Salary, "", job.Source, "")
	if err != nil {
		log.Fatal(err)
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	return err
}

func (driver *MysqlDB) IsJobExisted(job Job) bool {
	stmt, err := driver.Conn.Prepare(
		"SELECT count(*) FROM JOBS " +
			"WHERE SOURCE = ? AND SOURCE_ID = ?")

	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	row := stmt.QueryRow(job.Source,job.SourceID)
	var count int
	err = row.Scan(&count)
	if count > 0 {
		return true
	}
	return false
}