package crawler

import (
	"encoding/json"
	"fmt"
)

const (
	JobSourceTopdev = "topdev"
	JobSourceItviec = "itviec"
	JobSourceVietnamwork = "vietnamworks"
	JobSourceDevDes = "devdes"

	JobSelectorItviec = ""
	JobSelectorTopdev = ""

	DomainItviec = "itviec.com"
	DomainTopdev = ""
)

type Job struct {
	Title string
	Description string
	Benefits string
	Tags []string
	Address []string
	Salary string
	Source string
	SourceID string
	Company string
}

func (job Job) ToString() string {
	jsonEncoded, err := json.Marshal(job)
	if err != nil {
		fmt.Println(err);
		return ""
	}
	return string(jsonEncoded)
}

type Company struct {
	Name string
	Description string
}

