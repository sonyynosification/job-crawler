package crawler

import (
	"log"
	"testing"
)

var localDbProp = DBConnectionProperties{
	Host : "localhost",
	Port : 3306,
	User : "root",
	Password : "",
	DbName : "godb",
}

var mysqlDb MysqlDB

func init() {
	mysqlDb.Connect(localDbProp)
}

func TestMySQLDB_Connect(t *testing.T) {
	conn,err := mysqlDb.Connect(localDbProp)
	if err != nil {
		log.Println("Connection failed", err)
		t.Fail()
	}
	if conn == nil {
		log.Println("Connection is nil")
		t.Fail()
	}

	rows, err := conn.Query("select * from tmp")
	if err != nil {
		log.Println("Query failed", err)
		t.Fail()
	}

	for rows.Next() {
		var row int
		err = rows.Scan(&row)
		if err != nil {
			log.Fatal(err)
			t.Fail()
		}
		log.Println(row)
	}
}

func TestMysqlDB_AddJob(t *testing.T) {
	job1 := &Job{
		Title: "Job title 1",
		Description: "Job description xyz",
		Address: []string{"Ho Chi Minh"},
		Source: JobSourceItviec,
	}

	if mysqlDb.AddJob(*job1) != nil {
		log.Fatal("Add new job failed")
		t.Fail()
	}

}

// Delete tables before test
func TestMysqlDB_IsJobExisted(t *testing.T) {
	job1 := &Job{
		Title: "Job title 1",
		Description: "Job description xyz",
		Address: []string{"Ho Chi Minh"},
		Source: JobSourceItviec,
	}

	if mysqlDb.IsJobExisted(*job1) {
		log.Fatal("Job should not exist")
		t.FailNow()
	}
	mysqlDb.AddJob(*job1)
	if !mysqlDb.IsJobExisted(*job1) {
		log.Fatal("Job should exist")
	}
}
