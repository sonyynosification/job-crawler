package crawler



func StringInArray(str string, arr []string) bool {
	for _, i := range arr {
		if str == i {
			return true
		}
	}
	return false
}

