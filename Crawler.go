package main

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/debug"
	"time"
)

func main() {
	const url = "https://itviec.com/"
	//TestRandomDelay()
	TestGoColly()
}

func TestGoColly() {
	c := colly.NewCollector(
		colly.AllowedDomains("itviec.com"),
		colly.MaxDepth(1),
		//colly.Debugger(&debug.LogDebugger{}),
		//colly.Async(true),
	)

	c.Limit(&colly.LimitRule{
		DomainGlob: "*",
		//Parallelism: 2,
		RandomDelay: 5 * time.Second,
	})

	c.OnHTML(".first-group .job", func (e *colly.HTMLElement) {
		fmt.Println(e.DOM.Html())
	})

	// Find jobs
	c.OnHTML(".first-group .job .job_content .job__description .job__body .details .title a", func(e *colly.HTMLElement) {
		//PrintJobs(e)
	})

	// Find and visit more jobs links
	c.OnHTML("a.more-jobs-link", func(e *colly.HTMLElement) {

		e.Request.Visit(e.Attr("href"))
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.Visit("https://itviec.com/it-jobs")

	c.Wait()
}

func PrintJobs(e *colly.HTMLElement) {
	fmt.Println(e.Text)
}

func TestRandomDelay() {
	url := "https://httpbin.org/delay/2"

	// Instantiate default collector
	c := colly.NewCollector(
		// Attach a debugger to the collector
		colly.Debugger(&debug.LogDebugger{}),
		colly.Async(true),
	)

	// Limit the number of threads started by colly to two
	// when visiting links which domains' matches "*httpbin.*" glob
	c.Limit(&colly.LimitRule{
		DomainGlob:  "*httpbin.*",
		Parallelism: 2,
		RandomDelay: 5 * time.Second,
	})

	// Start scraping in four threads on https://httpbin.org/delay/2
	for i := 0; i < 4; i++ {
		c.Visit(fmt.Sprintf("%s?n=%d", url, i))
	}
	// Start scraping on https://httpbin.org/delay/2
	c.Visit(url)
	// Wait until threads are finished
	c.Wait()

}